﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwithStatement_WithUserInpit
{
    class Program
    {
        static void Main(string[] args)
        {
            var km = 0.0;
            var ml = 0.0;

            Console.WriteLine("What would you like to convert?");
            Console.WriteLine("1: Miles");
            Console.WriteLine("2: Kilometers");

            var selection = Console.ReadLine();

            switch (selection)
            {
                case "1":
                    Console.WriteLine("How many Miles would you like to convert to Kilometers?");
                    ml = int.Parse(Console.ReadLine());
                    km = ml * 1.609344;
                    Console.WriteLine(km);
                    break;

                case "2":
                    Console.WriteLine("How many Kilometers would you like to convert to Miles?");
                    km = int.Parse(Console.ReadLine());
                    ml = km * 0.621371;
                    Console.WriteLine(ml);
                    break;

                default:
                    Console.WriteLine("You selected something invalid");
                    break;

            }
        }
    }
}
